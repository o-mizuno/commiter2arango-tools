/*global module, require*/
(function (module, require) {
    "use strict";
    module.exports = function (grunt) {
        grunt.initConfig({
            "jslint": {
                "dev": {
                    "src": ["src/**/*.js"]
                }
            },
            "connect": {
                "dev": {
                    "options": {
                        "hostname": "127.0.0.1",
                        "livereload": true
                    }
                }
            },
            "watch": {
                "dev": {
                    "files": ["src/**/*.js"],
                    "tasks": ["jslint:dev"]
                }
            }
        });
        require('matchdep').filterDev('grunt-*').forEach(grunt.loadNpmTasks);
        grunt.registerTask("dev", "development", [
            "connect:dev",
            "watch:dev"
        ]);
    };
}(module, require));
