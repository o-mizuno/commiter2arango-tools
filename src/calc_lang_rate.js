/*global require, process, console, parseInt, parseFloat, Math*/
/*jslint nomen: true*/
(function (require, process, console, parseInt, parseFloat, Math) {
    "use strict";
    var get = require("./langcommiter").getLangCommiter,
        fs = require("fs"),
        usermax = parseInt(process.argv[2], 10),
        rateBacklash = parseFloat(process.argv[3], 10),
        outfname = process.argv[4],
        // cb should have the same parameter as getLangCommiter callback.
        excludeSingleEntity = function (cb) {
            return function (users, langs, edges) {
                var usersRet = users.slice(),
                    langsRet = langs.slice(),
                    edgesRet = edges.slice();
                users.forEach(function (user) {
                    var filteredEdge = edges.filter(function (edge) {
                        return edge._from === user._id;
                    });
                    if (filteredEdge.length < 2) {
                        usersRet.splice(usersRet.indexOf(user), 1);
                        if (filteredEdge.length === 1) {
                            edgesRet.splice(edgesRet.indexOf(filteredEdge[0]), 1);
                        }
                    }
                });
                langs.forEach(function (lang) {
                    var filteredEdge = edges.filter(function (edge) {
                        return edge._to === lang._id;
                    });
                    if (filteredEdge.length < 1) {
                        langsRet.splice(langsRet.indexOf(lang), 1);
                    }
                });
                cb(usersRet, langsRet, edgesRet);
            };
        };
    get(excludeSingleEntity(function (users, langs, edges) {
        var result = [];
        users.forEach(function (user) {
            var correspondingEdge = edges.filter(function (edge) {
                return edge._from === user._id;
            }),
                edgeIndex1,
                edgeIndex2,
                rate,
                edge1,
                lang1,
                edge2,
                lang2,
                result_topush,
                result_el,
                result_index,
                edge1Filter = function () {
                    return function (lang) {
                        return lang._id === edge1._to;
                    };
                },
                edge2Filter = function () {
                    return function (lang) {
                        return lang._id === edge2._to;
                    };
                },
                resultFilter = function (lang1, lang2) {
                    return function (el) {
                        return (el.languages[0] === lang1 &&
                                    el.languages[1] === lang2) ||
                                (el.languages[1] === lang1 &&
                                     el.languages[0] === lang2);
                    };
                },
                reversed = false;
            for (edgeIndex1 = 0;
                     edgeIndex1 < correspondingEdge.length - 1;
                     edgeIndex1 += 1) {
                edge1 = correspondingEdge[edgeIndex1];
                lang1 = langs.filter(edge1Filter())[0].language;
                for (edgeIndex2 = edgeIndex1 + 1;
                         edgeIndex2 < correspondingEdge.length;
                         edgeIndex2 += 1) {
                    edge2 = correspondingEdge[edgeIndex2];
                    lang2 = langs.filter(edge2Filter())[0].language;
                    rate = (edge1.add + edge1.del) / (edge2.add + edge2.del);
                    if (rate > 1.0) {
                        rate = 1 / rate;
                    }
                    result_topush = {
                        "languages": [lang1, lang2],
                        "rate": rate,
                        "editLines": (edge1.add + edge1.del) + (edge2.add + edge2.del),
                        "add": [edge1.add, edge2.add],
                        "del": [edge1.del, edge2.del],
                        "people": {
                        }
                    };
                    result_topush.people[user._id] = {
                        "editLines": (edge1.add + edge1.del) + (edge2.add + edge2.del),
                        "add": [edge1.add, edge2.add],
                        "del": [edge1.del, edge2.del]
                    };
                    result_el = result.filter(resultFilter(lang1, lang2))[0];
                    result_index = result.indexOf(result_el);
                    if (result_index < 0) {
                        result.push(result_topush);
                    } else {
                        reversed = result_el.languages[1] !== lang1;
                        if (!reversed) {
                            result[result_index].add[1] += edge1.add;
                            result[result_index].add[0] += edge2.add;
                            result[result_index].del[1] += edge1.del;
                            result[result_index].del[0] += edge2.del;
                        } else {
                            result[result_index].add[0] += edge1.add;
                            result[result_index].add[1] += edge2.add;
                            result[result_index].del[0] += edge1.del;
                            result[result_index].del[1] += edge2.del;
                        }

                        result[result_index].editLines = (
                            result[result_index].add[0] +
                                result[result_index].add[1]
                        ) + (
                            result[result_index].del[0] +
                                result[result_index].del[1]
                        );
                        result[result_index].rate = (
                            result[result_index].add[0] +
                                result[result_index].del[0]
                        ) / (
                            result[result_index].add[1] +
                                result[result_index].del[1]
                        );
                        if (result[result_index].rate > 1.0) {
                            result[result_index].rate = 1.0 / result[result_index].rate;
                        }
                        if (Object.keys(
                                result[result_index].people
                            ).indexOf(user._id) < 0) {
                            result[result_index].people[user._id] = {
                                "add": [
                                    (reversed ? edge1.add : edge2.add),
                                    (reversed ? edge2.add : edge1.add)
                                ],
                                "del": [
                                    (reversed ? edge1.del : edge2.del),
                                    (reversed ? edge2.del : edge1.del)
                                ]
                            };
                        } else {
                            if (reversed) {
                                result[result_index].people[user._id].add[1] = edge1.add;
                                result[result_index].people[user._id].add[0] = edge2.add;
                                result[result_index].people[user._id].del[1] = edge1.del;
                                result[result_index].people[user._id].del[0] = edge2.del;
                            } else {
                                result[result_index].people[user._id].add[0] = edge1.add;
                                result[result_index].people[user._id].add[1] = edge2.add;
                                result[result_index].people[user._id].del[0] = edge1.del;
                                result[result_index].people[user._id].del[1] = edge2.del;
                            }
                        }
                    }
                }
            }
        });
        result.sort(function (a, b) {
            var rateDiff = Math.abs(a.rate - b.rate);
            if (rateDiff <= rateBacklash) {
                return b.editLines - a.editLines;
            }
            return b.rate - a.rate;
        });
        if (outfname) {
            fs.writeFile(outfname, JSON.stringify(result), function (err) {
                if (err) {
                    throw err;
                }
                console.log("saved");
            });
        } else {
            console.log(JSON.stringify(result, undefined, 2));
        }
    }), usermax);
}(require, process, console, parseInt, parseFloat, Math));
