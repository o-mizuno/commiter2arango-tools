/*global require, console, exports*/
/*jslint nomen: true*/
(function (require, exports) {
    "use strict";
    /*
     * callback should be a function that has users, langs, and edges as arguments
     * Note that the arguments are in order.
     */
    exports.getLangCommiter = function (callback, usermax) {
        var arango = require("arangojs"),
            host = "127.0.0.1",
            port = 8529,
            lang = [],
            user = [],
            edge = [],
            lang_completed = false,
            edge_completed = false,
            db = arango.Connection("http://" + host + ":" + port + "/langcommiter"),
            // getEdge must be called after user
            getEdge = function () {
                var count = 0;
                user.forEach(function (el) {
                    db.query.for(
                        "l"
                    ).in(
                        "Learnt"
                    ).filter(
                        "l._from == @person"
                    ).return("l").exec({
                        "person": "People/" + el._key
                    }).then(function (data) {
                        edge.push.apply(edge, data);
                        if (count === user.length - 1) {
                            edge_completed = true;
                            if (lang_completed) {
                                callback(user, lang, edge);
                            }
                        }
                        count += 1;
                    });
                });
            };
        db.document.list("People").then(function (data) {
            var count = 0;
            data.documents.forEach(function (path, index) {
                if (usermax > 0 && index >= usermax) {
                    return;
                }
                var id = path.replace(/\/_api\/document\//g, "");
                db.document.get(id).then(function (doc) {
                    user.push(doc);
                    if (count === data.documents.length - 1 ||
                            (usermax > 0 && count === usermax - 1)) {
                        getEdge();
                    }
                    count += 1;
                });
            });
        });
        db.document.list("Languages").then(function (data) {
            var count = 0;
            data.documents.forEach(function (path) {
                var id = path.replace(/\/_api\/document\//g, "");
                db.document.get(id).then(function (doc) {
                    lang.push(doc);
                    if (count === data.documents.length - 1) {
                        lang_completed = true;
                        if (edge_completed) {
                            callback(user, lang, edge);
                        }
                    }
                    count += 1;
                });
            });
        });
    };
}(require, exports));
