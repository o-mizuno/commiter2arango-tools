/*global angular*/
(function (angular) {
    "use strict";
    angular.module("miner", [
        "ngResource"
    ]).factory("Result", function ($resource) {
        return $resource("src/final_result.json", {}, {
            "get": {
                "isArray": true
            }
        });
    }).filter("objLength", function () {
        return function (input) {
            return Object.keys(input).length;
        };
    }).controller("table", function ($scope, Result) {
        $scope.items = Result.get();
        $scope.numCommiterMin = function (el) {
            if ($scope.minNumCommiter) {
                return Object.keys(el.people).length >= $scope.minNumCommiter;
            }
            return true;
        };
    });
}(angular));
