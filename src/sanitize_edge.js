/*global require, console*/
/*jslint nomen: true*/
(function (require, console) {
    "use strict";
    var arango = require("arangojs"),
        host = "127.0.0.1",
        port = 8529,
        db = arango.Connection("http://" + host + ":" + port + "/langcommiter"),
        edges = [];
    db.document.list("Learnt").then(function (data) {
        var ids = data.documents.map(function (el) {
            return el.replace(/^\/_api\/edge\//, "");
        }), counter = 0, callback = function () {
            counter = 0;
            var l1Index,
                l2Index,
                filteredEdges = {},
                l1,
                l2,
                dupKey,
                dupEdge,
                removeEdge = function () {
                    return function (id) {
                        db.edge.delete("Learnt/" + id).then(function () {
                            console.log("Deleted Edge: " + id);
                        });
                    };
                };
            for (l1Index = 0; l1Index < edges.length; l1Index += 1) {
                l1 = edges[l1Index];
                for (l2Index = l1Index + 1; l2Index < edges.length; l2Index += 1) {
                    l2 = edges[l2Index];
                    if (l1._from === l2._from && l1._to === l2._to) {
                        if (filteredEdges.hasOwnProperty(l1._from + "," + l1._to)) {
                            filteredEdges[l1._from + "," + l1._to].add += l2.add;
                            filteredEdges[l1._from + "," + l1._to].del += l2.del;
                            filteredEdges[l1._from + "," + l1._to].duplicate.push(
                                l2._key
                            );
                        } else {
                            filteredEdges[l1._from + "," + l1._to] = {
                                "_from": l1._from,
                                "_to": l1._to,
                                "add": l1.add + l2.add,
                                "del": l1.del + l2.del,
                                "duplicate": [l1._key, l2._key]
                            };
                        }
                        console.log("Duplication Edge Found: " + l1._from + " -> " + l1._to +
                                    " Key ID = (" + l1._key + ", " + l2._key + ")");
                    }
                }
            }
            console.log("Collected.");
            for (dupKey in filteredEdges) {
                if (filteredEdges.hasOwnProperty(dupKey)) {
                    console.log(dupKey);
                    dupEdge = filteredEdges[dupKey];
                    dupEdge.duplicate.forEach(removeEdge());
                    db.edge.create("Learnt", dupEdge._from, dupEdge._to, {
                        "add": dupEdge.add,
                        "del": dupEdge.del
                    });
                }
            }
        };
        ids.forEach(function (id) {
            db.edge.get(id).then(function (edge) {
                edges.push(edge);
                if (counter === ids.length - 1) {
                    callback();
                }
                counter += 1;
            });
        });
    });
}(require, console));