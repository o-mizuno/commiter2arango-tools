/*#!/usr/bin/node*/

/*global require, process, console, parseInt*/
/*jslint nomen: true*/
(function (require, process, console, parseInt) {
    "use strict";
    var lang = require("./langcommiter"),
        fs = require("fs"),
        graphviz = require("graphviz"),
        usermax = parseInt(process.argv[2], 10);
    lang.getLangCommiter(function (user, lang, edge) {
        var g = graphviz.graph("G"),
            hasEdge = function (userOrlang, type) {
                var filtered = [];
                if (type === "person") {
                    filtered = edge.filter(function (el) {
                        return el._from === "People/" + userOrlang._key;
                    });
                } else if (type === "lang") {
                    filtered = edge.filter(function (el) {
                        return el._to === "Languages/" + userOrlang._key;
                    });
                }
                return filtered.length > 0 ? true : false;
            },
            drawUser = function (callback) {
                var count = 0;
                user.forEach(function (el) {
                    if (hasEdge(el, "person")) {
                        el.node = g.addNode("People/" + el._key, {
                            "style": "filled",
                            "label": (el.name + " (" + el.email + ")").replace(/"/g, "\\\""),
                            "color": "transparent"
                        });
                        console.log("Added User Node: " + el.name + " (" + el.email + ")");
                    }
                    if (count === user.length - 1) {
                        callback();
                    }
                    count += 1;
                });
            },
            drawLanguages = function (callback) {
                var count = 0;
                lang.forEach(function (el) {
                    if (hasEdge(el, "lang")) {
                        el.node = g.addNode("Languages/" + el._key, {
                            "style": "filled",
                            "label": el.language.replace(/"/g, "\\\""),
                            "color": "magenta"
                        });
                        console.log("Added Langauge Node: " + el.language);
                    }
                    if (count === lang.length - 1) {
                        callback();
                    }
                    count += 1;
                });
            },
            drawEdge = function (callback) {
                var count = 0;
                edge.forEach(function (el) {
                    if (g.getNode(el._from)) {
                        g.addEdge(el._from, el._to, {
                            "label": "add: " + el.add + " del: " + el.del
                        });
                        console.log("Added Edge (" + el._from + " -> " + el._to + ")");
                    }
                    if (count === edge.length - 1) {
                        callback();
                    }
                    count += 1;
                });
            },
            completedDrawUser = false,
            completedDrawLanguage = false,
            edgeCallback = function () {
                fs.writeFile("result.dot", g.to_dot(), function () {
                    console.log("completed");
                });
            };
        drawUser(function () {
            completedDrawUser = true;

            if (completedDrawLanguage) {
                drawEdge(edgeCallback);
            }
        });
        drawLanguages(function () {
            completedDrawLanguage = true;

            if (completedDrawUser) {
                drawEdge(edgeCallback);
            }
        });
    }, usermax);
}(require, process, console, parseInt));